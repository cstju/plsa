# -*- coding: utf-8 -*-
# @Author: mac@lab538
# @Date:   2016-12-11 11:21:38
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-12-16 11:38:13

from random import gammavariate
from random import random

"""
Samples from a Dirichlet distribution with parameter @alpha using a Gamma distribution
Reference: 
http://en.wikipedia.org/wiki/Dirichlet_distribution
http://stackoverflow.com/questions/3028571/non-uniform-distributed-random-array
"""
def Dirichlet(alpha):#生成狄利克雷分布样本
    sample = [gammavariate(a,1) for a in alpha]#获取概率分布服从Gamma distribution，参数为a和1的随机数样本
    sample = [v/sum(sample) for v in sample]#将样本sample的值归一化
    return sample
    
"""
Normalize a vector to be a probablistic representation
"""
def normalize(vec):
    s = sum(vec)
    """
    assert是用于检测是否有bug的语句
    >>> assert True
    >>> assert False
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    AssertionError
    """

    assert(abs(s) != 0.0) # the sum must not be 0
    """
    if abs(s) < 1e-6:
        print "Sum of vectors sums almost to 0. Stop here."
        print "Vec: " + str(vec) + " Sum: " + str(s)
        assert(0) # assertion fails
    """
        
    for i in range(len(vec)):
        assert(vec[i] >= 0) # element must be >= 0
        vec[i] = vec[i] * 1.0 / s
    #return vec
        
"""
Choose a element in @vec according to a specified distribution @pr
Reference:
http://stackoverflow.com/questions/4437250/choose-list-variable-given-probability-of-each-variable
"""
def choose(vec, pr):#按照给定的概率分布pr来选择vec
    assert(len(vec) == len(pr))
    # normalize the distributions
    normalize(pr)
    r = random()
    index = 0
    while (r > 0):
        r = r - pr[index]
        index = index + 1
    return vec[index-1]
    
if __name__ == "__main__":
    # This is a test
    print Dirichlet([1,1,1]);
