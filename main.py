# -*- coding: utf-8 -*-
# @Author: mac@lab538
# @Date:   2016-12-11 11:21:38
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-12-19 21:25:41
import os
import glob
import sys
from operator import itemgetter # for sort
import cPickle

import plsa

STOP_WORDS_SET = set()

def print_topic_word_distribution(corpus, number_of_topics, topk, filepath):
    """
    Print topic-word distribution to file and list @topk most probable words for each topic
    """
    print "Writing topic-word distribution to file: " + filepath
    V = len(corpus.vocabulary) # size of vocabulary
    assert(topk < V)
    f = open(filepath, "w")
    for k in range(number_of_topics):
        word_prob = corpus.topic_word_prob[k, :]
        word_index_prob = []
        for i in range(V):
            word_index_prob.append([i, word_prob[i]])
        word_index_prob = sorted(word_index_prob, key=itemgetter(1), reverse=True) # sort by word count
        f.write("Topic #" + str(k) + ":\n")
        for i in range(topk):
            index = word_index_prob[i][0]
            f.write(corpus.vocabulary[index] + " ")
        f.write("\n")
        
    f.close()
    
def print_document_topic_distribution(corpus, number_of_topics, topk, filepath):
    """
    Print document-topic distribution to file and list @topk most probable topics for each document
    """
    print "Writing document-topic distribution to file: " + filepath
    assert(topk <= number_of_topics)
    f = open(filepath, "w")
    D = len(corpus.documents) # number of documents
    for d in range(D):
        topic_prob = corpus.document_topic_prob[d, :]
        topic_index_prob = []
        for i in range(number_of_topics):
            topic_index_prob.append([i, topic_prob[i]])
        topic_index_prob = sorted(topic_index_prob, key=itemgetter(1), reverse=True)
        f.write("Document #" + str(d) + ":\n")
        for i in range(topk):
            index = topic_index_prob[i][0]
            f.write("topic" + str(index) + " ")
        f.write("\n")
        
    f.close()
        
def main(num_topics,max_itera):
    #print "Usage: python ./main.py <number_of_topics> <maxiteration>"
    # load stop words list from file
    stopwordsfile = open("./stopwords.txt", "r")
    """
    stopwords.txt文件中存储的是一些常用，没有代表性的单词
    在构建单词库时，不考虑这些单词
    """
    for word in stopwordsfile: # a stop word in each line
        word = word.replace("\n", '')#将原来每行中的\n，\r\n 都删除(巧妙的采用替换replace函数)
        word = word.replace("\r\n", '')
        STOP_WORDS_SET.add(word)#构建单词集
    
    corpus = plsa.Corpus() # instantiate corpus
    # iterate over the files in the directory.
    document_paths = ['./texts/grimm_fairy_tales', './texts/tech_blog_posts', './texts/nyt']#共有3个文本路径
    #document_paths = ['./test/']
    for document_path in document_paths:
        for document_file in glob.glob(os.path.join(document_path, '*.txt')):#把每个文档都读取
            """
        os.path.join 将多个路径拼接
        >>> document_path = './texts/tech_blog_posts'
        >>> print os.path.join(document_path, '*.txt')
        >>> ./texts/tech_blog_posts/*.txt

        glob.glob() 返回按照某种规则查找的文件名
        假如在某一个文件夹下有三个文件：1.gif,2.txt,carf.gif
        >>> glob.glob('./[0-9].*')
        ['./1.gif', './2.txt']
        >>> glob.glob('*.gif')
        ['1.gif', 'card.gif']
        >>> glob.glob('?.gif')
        ['1.gif']
        在本程序中：
        >>> print glob.glob('./texts/tech_blog_posts/*.txt')
        >>> ['./texts/tech_blog_posts/codecademy_surges_to_200k_users.txt', './texts/tech_blog_posts/twitter_url_shortener_analytics.txt']
            """
            document = plsa.Document(document_file) # 这里每一个document都是plsa.Document这个类的一个对象
            document.split(STOP_WORDS_SET) # 每一篇文章（每一个document对象）都会构建一个self.words单词表
            corpus.add_document(document) # 将每一篇文章（对象）加到文章列表中

    corpus.build_vocabulary()
    print "Vocabulary size:" + str(len(corpus.vocabulary))#单词总数
    print "Number of documents:" + str(len(corpus.documents))#文章总数
    
    number_of_topics = int(num_topics)
    max_iterations = int(max_itera)
    """
    for test, fixed values are assigned, where number_of_documents = 3, vocabulary_size = 15
    number_of_topics = 5
    max_iterations = 20
    """
    corpus.plsa(number_of_topics, max_iterations)
    
    #print corpus.document_topic_prob
    #print corpus.topic_word_prob
    #cPickle.dump(corpus, open('./models/corpus.pickle', 'w'))
    
    print_topic_word_distribution(corpus, number_of_topics, 20, "./topic-word.txt")
    print_document_topic_distribution(corpus, number_of_topics, 3, "./document-topic.txt")
    
if __name__ == "__main__":
    num_topics = 7
    max_itera = 20
    main(num_topics,max_itera)
