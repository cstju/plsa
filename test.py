# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-12-16 10:42:52
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-12-19 11:59:29

from random import gammavariate
from random import random
import numpy
import os
import glob
import re
def normalize(vec):
    s = sum(vec)
    """
    assert是用于检测是否有bug的语句
    >>> assert True
    >>> assert False
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    AssertionError
    """

    assert(abs(s) != 0.0) # the sum must not be 0
    """
    if abs(s) < 1e-6:
        print "Sum of vectors sums almost to 0. Stop here."
        print "Vec: " + str(vec) + " Sum: " + str(s)
        assert(0) # assertion fails
    """
        
    for i in range(len(vec)):
        assert(vec[i] >= 0) # element must be >= 0
        vec[i] = vec[i] * 1.0 / s
    #return vec

#self.a = [1,2,3,4,5,6,7]
#print normalize(self.a)
# document_path = './texts/grimm_fairy_tales'
# print os.path.join(document_path, '*.txt')
# print glob.glob('./texts/tech_blog_posts/*.txt')
# string = "blah, lots  #, # of , # spaces, #he#re "
# mylist = string.split(',')
# mylist2 = [x.strip() for x in mylist]
# mylist3 = [x.strip("#") for x in mylist2]
# mylist4 = [x.strip() for x in mylist3]
# print mylist
# print mylist2
# print mylist3
# print mylist4

# string = "00this is 00 string exam00ple....wow!!!000";
# print string.strip('0')
# word = "sdasf'sa'"
# WORD_REGEX = "^[a-z']$"
# if re.match(WORD_REGEX, word):
	# print word
topic_prob = numpy.zeros([2, 3, 4], dtype=numpy.float)
print topic_prob
topic_prob[1][2] = 5
print topic_prob
a = numpy.array([1,2,3])
b = numpy.array([4,5,6])
print a,b
print a*b
print a.shape